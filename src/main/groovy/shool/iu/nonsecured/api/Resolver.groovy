package shool.iu.nonsecured.api

import java.time.Duration
import java.util.function.Function

import org.reactivestreams.Publisher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver
import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver

import reactor.core.publisher.Flux


@Component
class TaskResolver implements GraphQLResolver<Task> {

	@Autowired
	UserInfoRepository userInfoRepository
	@Autowired
	ProjectRepository projectReposity
	@Autowired
	TaskRepository taskRepository

	UserInfo getAssignTo(Task task) {
		userInfoRepository.findById(task.assignee)
	}

	UserInfo getReportTo(Task task) {
		userInfoRepository.findById(task.reporter)
	}

	Project getProject(Task task) {
		projectReposity.findByProjectCode(task.projectCode)
	}
	
	Task getParent(Task task) {
		taskRepository.findByTaskCode(task.parentCode)
	}
	
	List<Task> getSubTasks(Task task){
		taskRepository.findByParentCode(task.taskCode)
	}
}

@Component
class ProjectResolver implements GraphQLResolver<Project> {

	@Autowired
	UserInfoRepository userInfoRepository

	@Autowired
	TaskRepository taskRepository
		
	UserInfo getUser(Project project) {
		userInfoRepository.findById(project.createdBy)
	}

	List<Task> getTasks(Project project){
		taskRepository.findByProjectCode(project.getProjectCode())
	}
	
	List<UserInfo> getMembers(Project project){
		userInfoRepository.findByProjectCodes([project.getProjectCode()])
	}
}

@Component
class UserInfoResolver implements GraphQLResolver<UserInfo> {

	@Autowired
	OfficeRepository officeRepository
	@Autowired
	ProjectRepository projectReposity

	Office getOffice(UserInfo userInfo) {
		officeRepository.findById(userInfo.officeCode)
	}

	List<Project> getProjects(UserInfo userInfo) {
		List<Project> list = new ArrayList()
		for (String id in userInfo.projectCodes) {
			list.add(projectReposity.findById(id).get())
		}
		list
	}
}

@Component
class OfficeResolver implements GraphQLResolver<Office> {
}

@Component
class ActivityResolver implements GraphQLResolver<Activity> {

	@Autowired
	UserInfoRepository userInfoRepository
	@Autowired
	ProjectRepository projectReposity
	@Autowired
	TaskRepository taskRepository

	UserInfo getUser(Activity activity) {
		userInfoRepository.findById(activity.userId)
	}

	Project getProject(Activity activity) {
		projectReposity.findById(activity.projectCode)
	}

	Task getTask(Activity activity) {
		taskRepository.findById(activity.taskCode)
	}
}

@Component
class Query implements GraphQLQueryResolver {

	@Autowired
	UserInfoRepository userInfoRepository
	@Autowired
	ProjectRepository projectReposity
	@Autowired
	TaskRepository taskRepository
	@Autowired
	ActivityRepository activityRepository
	@Autowired
	OfficeRepository officeRepository

	List<Project> getAllProjects() {
		projectReposity.findAll()
	}
	List<Task> getAllTasks() {
		taskRepository.findAll()
	}
	List<Office> getAllOffices() {
		officeRepository.findAll()
	}
	List<UserInfo> getAllUserInfos() {
		userInfoRepository.findAll()
	}
	List<Activity> getAllActivities() {
		activityRepository.findAll()
	}

	Project getProject(id){
		projectReposity.findById(id).get()
	}

	Task getTask(id){
		taskRepository.findById(id).get()
	}
}

@Component
class Subscription implements GraphQLSubscriptionResolver {

	@Autowired
	UserInfoRepository userInfoRepository
	@Autowired
	ProjectRepository projectReposity
	@Autowired
	TaskRepository taskRepository
	@Autowired
	ActivityRepository activityRepository
	@Autowired
	OfficeRepository officeRepository

	Publisher<List<Project>> getAllProjects() {
		Flux.interval(Duration.ofSeconds(1)).map({m -> projectReposity.findAll().asList()})
	}
	
	Publisher<Project> getProject(id) {
		Flux.interval(Duration.ofSeconds(1)).map({m -> projectReposity.findById(id).get()})
	}
	
	Publisher<Task> getTask(id) {
		Flux.interval(Duration.ofSeconds(1)).map({m -> taskRepository.findById(id).get()})
	}
	
}