package shool.iu.nonsecured.api

import org.springframework.context.annotation.Configuration
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.domain.AuditorAware
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.repository.PagingAndSortingRepository

import com.fasterxml.jackson.annotation.JsonFormat

enum Status{
	OP, PR, CP
}

enum State{
	AP, DC, PD
}

@Document
class UserInfo {
	@Id
	String id
	String username
	String name
	Date dob
	String phone
	String officeCode
	List<String> projectCodes
}

interface UserInfoRepository extends PagingAndSortingRepository<UserInfo, String> {
	def List<UserInfo> findByOfficeCode(String officeCode)
	def List<UserInfo> findByProjectCodes(List<String> projectCodes)
}

@Document
class Project{
	@Id
	String id
	String name
	String description
	Date startDate
	Status status
	State state
	Date createdDate
	Date deadline
	String projectCode
	String createdBy
}

class ProjectInput {
	String name
	String description
	String projectCode
	Date startDate
	Date deadline
}

interface ProjectRepository extends PagingAndSortingRepository<Project, String> {
	def List<Project> findByCreatedBy(String createdBy)
	def Project findByProjectCode(String projectCode)
}

@Document
class Task{
	@Id
	String id
	String name
	String description
	Date startDate
	Date deadline
	Status status
	State state
	String taskCode
	Date createdDate
	String projectCode
	String assignee
	String reporter
	String parentCode
}

class TaskInput{
	String name
	String description
	String taskCode
	Date startDate
	Date deadline
	String projectCode
	String assignee
	String reporter
	String parentCode
}

interface TaskRepository extends PagingAndSortingRepository<Task, String> {
	def List<Task> findByProjectCode(String projectCode)
	def List<Task> findByAssignee(String assignee)
	def List<Task> findByReporter(String reporter)
	def List<Task> findByParentCode(String ParentCode)
	def Task findByTaskCode(String taskCode)
}

@Document
class Office{
	@Id
	String id
	String name
	String officeCode
	Date createdDate
}

class OfficeInput {
	String name
	String officeCode
}


interface OfficeRepository extends PagingAndSortingRepository<Office, String> {
	def List<Office> findByCreatedDate(Date createdDate)
}

@Document
class Activity{
	@Id
	String id
	String description
	String function
	Date createdDate
	String userId
	String projectCode
	String taskCode
	
}

class ActivityInput {
	String description
	String function	
}

interface ActivityRepository extends PagingAndSortingRepository<Activity, String> {
	def List<Activity> findByUserId(String userId)
	def List<Activity> findByProjectCode(String projectCode)
	def List<Activity> findByTaskCode(String taskCode)
}

