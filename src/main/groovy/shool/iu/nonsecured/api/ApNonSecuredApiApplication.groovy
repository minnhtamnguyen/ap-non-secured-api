package shool.iu.nonsecured.api

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ApNonSecuredApiApplication {

	static void main(String[] args) {
		SpringApplication.run(ApNonSecuredApiApplication, args)
	}

}
